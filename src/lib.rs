#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![deny(clippy::clone_on_ref_ptr)]

// #[macro_use]
extern crate alloc;
pub mod run;
pub mod utils;

// use embassy_executor::Spawner;
// use embassy_net::tcp::TcpSocket;
// use embassy_net::{Stack, StackResources};
// use embassy_serial::{AsyncDevice, Config};
// use embassy_time::{Duration, Timer};
// use embedded_hal::digital::v2::OutputPin;
// use std::net::Ipv4Addr;

// #[embassy_executor::main]
// async fn main(_spawner: Spawner) {
//     // Configure the serial port connected to the ESP32
//     let config = Config::default();
//     let mut serial = AsyncDevice::new(peripherals.UART0, config).unwrap();

//     // Configure the network stack
//     static mut RX_BUFFER: [u8; 1024] = [0; 1024];
//     static mut TX_BUFFER: [u8; 1024] = [0; 1024];
//     let stack = &mut Stack::new(
//         StackResources::new()
//             .tx_buffer(TX_BUFFER)
//             .rx_buffer(RX_BUFFER),
//     );

//     // Power on the modem (if necessary)
//     // ...

//     // Initialize the PPP connection
//     let mut ppp = embassy_net_ppp::Ppp::new(&mut serial, stack);

//     // Attach to the GPRS network
//     ppp.write_at("AT+CGATT=1\r").await.unwrap();
//     ppp.wait_for_ok().await.unwrap();

//     // Set the PDP context
//     ppp.write_at("AT+CGDCONT=1,\"IP\",\"cmnet\"\r")
//         .await
//         .unwrap();
//     ppp.wait_for_ok().await.unwrap();
//     // AT+CGDCONT=1,"IP","internet"
//     // Activate the PDP context
//     ppp.write_at("AT+CGACT=1,1\r").await.unwrap();
//     ppp.wait_for_ok().await.unwrap();

//     // Start PPP translation
//     ppp.write_at("ATD*99***1#\r").await.unwrap();
//     ppp.wait_for("CONNECT").await.unwrap();

//     // Wait for the connection to be established
//     ppp.wait_for("+++").await.unwrap();
//     Timer::after(Duration::from_secs(3)).await;

//     // Configure the network interface
//     let config = embassy_net::Config::dhcpv4(Default::default());
//     stack.configure(config).unwrap();

//     // Wait for the network to be configured
//     stack.wait_for_link().await;

//     // Get the assigned IP address
//     let ip = stack.get_ip_address().unwrap();
//     println!("IP address: {}", ip);

//     // Create a TCP socket
//     let mut socket = TcpSocket::new(stack, &mut [0; 1024]);
//     socket.bind(Ipv4Addr::UNSPECIFIED, 8080).unwrap();
//     socket.listen(1).unwrap();

//     loop {
//         // Accept a connection
//         let (_remote_addr, mut connection) = socket.accept().await.unwrap();

//         // Handle the connection
//         // ...
//     }
// }
