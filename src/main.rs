#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![deny(clippy::clone_on_ref_ptr)]

use alloc::vec::Vec;
use embassy_executor::Spawner;
use embassy_net::{Config, ConfigV4, Ipv4Address, Ipv4Cidr, Stack, StackResources};
use embassy_net_ppp::Runner;
use embassy_sync::{blocking_mutex::raw::NoopRawMutex, mutex::Mutex, signal::Signal};
use embassy_time::{with_timeout, Duration, Timer};
use esp_backtrace as _;
// use hal::uart::ClockSource;
use hal::{
    clock::{ClockControl, Clocks},
    delay::Delay,
    gpio::{GpioPin, Io},
    peripherals::{Peripherals, UART0, UART1},
    prelude::*,
    system::SystemControl,
    timer::{timg::TimerGroup, ErasedTimer, OneShotTimer},
    uart::{
        config::{Config as UartConfig, DataBits, Parity, StopBits},
        ClockSource, Uart,
    },
};
use log::{error, info, warn};
use ppp_gsm::{
    run::modem::{
        module::{SharedUart1, UartWrapper},
        parse::parse::{ModemResponse, ToSend},
    },
    singleton,
    utils::generate_random_seed,
};
use static_cell::StaticCell;
extern crate alloc;
use core::mem::MaybeUninit;

#[global_allocator]
static ALLOCATOR: esp_alloc::EspHeap = esp_alloc::EspHeap::empty();

fn init_heap() {
    const HEAP_SIZE: usize = 32 * 1024;
    static mut HEAP: MaybeUninit<[u8; HEAP_SIZE]> = MaybeUninit::uninit();

    unsafe {
        ALLOCATOR.init(HEAP.as_mut_ptr() as *mut u8, HEAP_SIZE);
    }
}

pub const READ_BUF_SIZE: usize = 64;
// When you are okay with using a nightly compiler it's better to use https://docs.rs/static_cell/2.1.0/static_cell/macro.make_static.html
macro_rules! mk_static {
    ($t:ty,$val:expr) => {{
        static STATIC_CELL: static_cell::StaticCell<$t> = static_cell::StaticCell::new();
        #[deny(unused_attributes)]
        let x = STATIC_CELL.uninit().write(($val));
        x
    }};
}

#[main]
async fn main(spawner: Spawner) {
    let peripherals = Peripherals::take();
    let system = SystemControl::new(peripherals.SYSTEM);

    // let clocks = ClockControl::max(system.clock_control).freeze();
    let clocks = singleton!(ClockControl::max(system.clock_control).freeze(), Clocks<'_>);

    let timg0 = TimerGroup::new(peripherals.TIMG0, &clocks, None);
    let timer0 = OneShotTimer::new(timg0.timer0.into());
    let timers = [timer0];
    let timers = mk_static!([OneShotTimer<ErasedTimer>; 1], timers);
    esp_hal_embassy::init(&clocks, timers);

    let delay = Delay::new(&clocks);
    init_heap();

    esp_println::logger::init_logger_from_env();

    // Init network device
    static STATE: StaticCell<embassy_net_ppp::State<4, 4>> = StaticCell::new();
    let state = STATE.init(embassy_net_ppp::State::<4, 4>::new());
    let (device, runner) = embassy_net_ppp::new(state);

    // Generate random seed
    let seed = generate_random_seed();

    // Init network stack
    static STACK: StaticCell<Stack<embassy_net_ppp::Device<'static>>> = StaticCell::new();
    static RESOURCES: StaticCell<StackResources<3>> = StaticCell::new();
    let stack = &*STACK.init(Stack::new(
        device,
        Config::default(), // don't configure IP yet
        RESOURCES.init(StackResources::<3>::new()),
        seed,
    ));
    let io = Io::new(peripherals.GPIO, peripherals.IO_MUX);

    static SIGNAL: StaticCell<Signal<NoopRawMutex, (usize, Vec<u8>)>> = StaticCell::new();
    let signal = &*SIGNAL.init(Signal::new());

    static MODEM_MUTEX: StaticCell<Mutex<NoopRawMutex, UartWrapper<'static>>> = StaticCell::new();
    let mut modem = UartWrapper::new(
        peripherals.UART0,
        peripherals.UART1,
        io.pins.gpio14,
        io.pins.gpio15,
        signal,
        &clocks,
    );
    let modem = &*MODEM_MUTEX.init(Mutex::new(modem));

    // spawner.spawn(uart_reader(modem)).unwrap();
    match spawner.spawn(uart_reader(modem)) {
        Ok(_) => info!("uart_reader task spawned successfully"),
        Err(e) => error!("Failed to spawn uart_reader task: {:?}", e),
    }
    // spawner.spawn(ppp_task(stack, runner, modem)).unwrap();
    match spawner.spawn(ppp_task(stack, runner, modem)) {
        Ok(_) => info!("ppp_task task spawned successfully"),
        Err(e) => error!("Failed to spawn ppp_task task: {:?}", e),
    }
    {
        let mut modem_guard = modem.lock().await;
        match modem_guard.init_with_retry().await {
            Ok(_) => info!("init completed successfully"),
            Err(e) => {
                error!("init failure: {:?}", e);
            }
        };
    }
    loop {
        log::info!("Hello world!");
        delay.delay(500.millis());
    }
}
use embedded_io_async::Read;

#[embassy_executor::task]
async fn ppp_task(
    stack: &'static Stack<embassy_net_ppp::Device<'static>>,
    mut runner: Runner<'static>,
    // mut modem: UartWrapper<'static>,
    modem: &'static Mutex<NoopRawMutex, UartWrapper<'static>>,
) -> ! {
    let config = embassy_net_ppp::Config {
        username: b"myuser",
        password: b"mypass",
    };
    Timer::after(Duration::from_millis(10)).await; // Adjust delay if needed

    let mut modem_guard = modem.lock().await;

    runner
        .run(&mut *modem_guard, config, |ipv4| {
            let Some(addr) = ipv4.address else {
                warn!("PPP did not provide an IP address.");
                return;
            };
            // let mut dns_servers = Vec::new();
            let mut dns_servers: heapless::Vec<Ipv4Address, 3> = heapless::Vec::new();

            for s in ipv4.dns_servers.iter().flatten() {
                let _ = dns_servers.push(Ipv4Address::from_bytes(&s.0));
            }
            let config = ConfigV4::Static(embassy_net::StaticConfigV4 {
                address: Ipv4Cidr::new(Ipv4Address::from_bytes(&addr.0), 0),
                gateway: None,
                dns_servers,
            });
            stack.set_config_v4(config);
        })
        .await
        .unwrap();
    unreachable!()
}
#[embassy_executor::task]
// async fn uart_reader(reader: &'static Mutex<NoopRawMutex, UartWrapper<'static>>) -> ! {
async fn uart_reader(modem: &'static Mutex<NoopRawMutex, UartWrapper<'static>>) -> ! {
    const MAX_BUFFER_SIZE: usize = 10 * READ_BUF_SIZE + 16;
    let mut rbuf: [u8; MAX_BUFFER_SIZE] = [0u8; MAX_BUFFER_SIZE];
    let mut offset = 0;

    info!("uart_reader task started");

    loop {
        info!("reader after loop");
        {
            let mut modem_guard = modem.lock().await;

            info!("rdr after loop");
            // Log UART configuration
            // info!("UART1 configuration: {:?}", modem_guard.uart1.get_config());
            // Perform a direct write-read test to ensure UART is working
            use embedded_io_async::Write;
            let test_data = b"AT\r\n";
            modem_guard.write(test_data).await.unwrap();
            info!("Sent test data: {:?}", test_data);

            info!("rdr before read()");
            match modem_guard.read(&mut rbuf[offset..]).await {
                Ok(len) => {
                    info!("rdr_guard after read()");
                    if len > 0 {
                        offset += len;
                        info!("Received {} bytes: {:?}", len, &rbuf[..offset]);

                        modem_guard.signal.signal((offset, rbuf[..offset].to_vec()));
                        offset = 0; // Reset the offset after signaling
                    } else {
                        info!("Received empty data (len = 0)");
                    }
                }
                Err(e) => {
                    error!("[uart_reader] RX Error: {:?}", e);
                }
            }
        }

        // Add a small delay to avoid busy looping too fast
        Timer::after(Duration::from_millis(1)).await; // Adjust delay if needed
    }
}

trait NewTrait<'a> {
    pub async fn send_at(
        &mut self,
        tosend: Option<&[u8]>,
        msg: Option<&str>,
        _timeout_ms: Option<u64>,
    );
    fn new(
        // uart: &'a mut Uart<'static, UART1, Async>
        _uart0_per: UART0,
        uart1_per: UART1,
        tx_pin: GpioPin<14>,
        rx_pin: GpioPin<15>,
        signal: &'static Signal<NoopRawMutex, (usize, Vec<u8>)>,
        clocks: &Clocks<'static>,
    ) -> Self;
}

impl<'a> NewTrait<'a> for UartWrapper<'a> {
    fn new(
        // uart: &'a mut Uart<'static, UART1, Async>
        _uart0_per: UART0,
        uart1_per: UART1,
        tx_pin: GpioPin<14>,
        rx_pin: GpioPin<15>,
        signal: &'static Signal<NoopRawMutex, (usize, Vec<u8>)>,
        clocks: &Clocks<'static>,
    ) -> Self {
        // let uart1 = {
        let (tx, rx) = {
            let config = UartConfig {
                baudrate: 115200,
                data_bits: DataBits::DataBits8,
                parity: Parity::ParityNone,
                stop_bits: StopBits::STOP1,
                clock_source: ClockSource::Apb,
                rx_fifo_full_threshold: 1,
                rx_timeout: Some(3),
            };

            config.rx_fifo_full_threshold(1);
            let uart =
                Uart::new_async_with_config(uart1_per, config, &clocks, tx_pin, rx_pin).unwrap();
            uart.split()
        };

        let modem_res = ModemResponse::new();
        let tosend = ToSend::new();

        Self {
            uart1: SharedUart1 { tx, rx },
            signal,
            buffer: [0; 256],
            read_pos: 0,
            write_pos: 0,
            modem_res,
            tosend,
        }
    }
    async fn send_at(
        &mut self,
        tosend: Option<&[u8]>,
        msg: Option<&str>,
        _timeout_ms: Option<u64>,
    ) {
        let msg_str = msg.unwrap_or("default");
        let tosend_f = tosend.unwrap_or(AT_CMD);

        info!(
            "\n\x1b[32m[{msg_str}]\x1b[0m writing command: {:?}",
            tosend_f
        );

        // Set the command to be sent
        self.tosend.set_to_sent(tosend_f);
        // Get the command after setting it
        let cmd = self.tosend.get_to_sent().to_vec(); // Clone the command to a local variable

        if cmd.is_empty() {
            info!("EMPTY: Command to send to modem is empty");
            return;
        }
        use embedded_io::Write;
        // Now we can safely call self.write without borrowing self.tosend
        if let Err(e) = self.write(&cmd).await {
            error!("UART1 write failure: {:?}", e);
        } else {
            info!("UART1 wrote [{:?}] successfully", cmd);
        }
    }
}
