// use super::{
//     errors::{Error, Result},
//     module::{FoundResponse, Modem},
//     parse::parse::{ModemResponse, Status},
// };

use super::{
    error::{Error, Result},
    module::FoundResponse,
};

#[derive(Clone)]
pub struct Apn<'a> {
    pub apn: &'a str,
    pub username: &'a str,
    pub password: &'a str,
}

impl<'a> Apn<'a> {
    pub const fn new(apn: &'a str) -> Self {
        Self {
            apn,
            username: "",
            password: "",
        }
    }

    pub fn get_apn_field(&self) -> &'a str {
        self.apn
    }
}

impl<'a> From<&'a str> for Apn<'a> {
    fn from(value: &'a str) -> Self {
        Apn::new(value)
    }
}

impl UartWrapper {
    pub async fn get_apn(&mut self) -> Result<Apn> {
        self.tosend.set_to_sent(b"AT+CSTT?\r\n");
        self.send_at(None, Some("get_apn"), None).await;

        let response = self.wait_response(None, None, None).await?;
        match response.1 {
            FoundResponse::M1 => Ok(self.modem_res.parse_apn(&response.0.response.data[..])?),
            _ => Err(Error::Custom("error getting apn")),
        }
    }
    pub async fn set_apn(
        &mut self,
        provider_name: &str,
        userid: &str,
        password: &str,
    ) -> Result<Status> {
        self.tosend.set_to_sent(
            alloc::format!(
                "AT+CSTT=\"{}\",\"{}\",\"{}\"\r\n",
                provider_name,
                userid,
                password
            )
            .as_bytes(),
        );

        self.send_at(None, Some("set_apn"), None).await;

        let response = self.wait_response(None, None, None).await?;
        match response.1 {
            FoundResponse::M1 => Ok(response.0.response.status),
            _ => Err(Error::Custom("error setting apn")),
        }
    }
}

impl ModemResponse {
    pub(crate) fn parse_apn(&self, apn_response: &[u8]) -> Result<Apn> {
        // +CSTT: \"CMNET\",  \"\", \"\""
        let apn = Apn::new("internet");
        Ok(apn)
    }
}
