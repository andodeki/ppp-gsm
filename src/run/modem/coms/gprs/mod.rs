use log::{error, info};

use crate::run::modem::{
    errors::{Error, Result},
    module::{FoundResponse, RegistrationStatus, UartWrapper},
    parse::parse::{ModemResponse, PdpState, Status},
};

#[derive(Debug, PartialEq)]
#[repr(u8)]
pub enum CIPSTATUS {
    IPINITIAL,
    IPSTART,
    IPCONFIG,
    IPIND,
    IPGPRSACT,
    IPSTATUS,
    TCPCONNECTING,
    IPCLOSE,
    CONNECTOK,
}
impl<'a> UartWrapper<'a> {
    /*
    AT+CGATT=1 ;Attach to the GPRS network, can also use parameter 0 to detach.
    OK ;Response, attach successful

    AT+CGDCONT=? ;Input test command for help information.
    +CGDCONT: (1..7), (IP,IPV6,PPP),(0..3),(0..4) OK ;Response, show the helpful information.

    // AT+CGDCONT=1,"IP","internet"
    AT+CGDCONT=1, "IP", "cmnet" ;Before active, use this command to set PDP context.
    OK ;Response. Set context OK.

    AT+CGACT=1,1 ;Active command is used to active the specified PDP context.
    OK ;Response, active successful.

    ATD*99***1# ;This command is to start PPP translation.
    CONNECT ;Response, when get this, the module has been set to data state.
            PPP data should be transferred after this response and anything input is treated as data.

    +++ ;This command is to change the status to online data state.
        Notice that before input this command, you need to wait for a
        three seconds’ break, and it should also be followed by 3 seconds’
        break, otherwise “+++” will be treated as data.

    ATH ;Use this command to return COMMAND state
    ok Response
    */

    pub async fn set_modem_gprs(&mut self) -> Result<(CIPSTATUS, Status)> {
        // Attempt to check network registration, handle potential errors
        self.check_network_registration().await.map_err(|e| {
            info!("Failed to check network registration: {:?}", e);
            Error::Custom("Network registration failed")
        })?;

        // Check attached network status
        match self.check_attached_network().await {
            Ok(s) => match s {
                PdpState::Activated => {
                    info!("Already attached to the network");
                    let pdpstatus = self.set_pdp_parameter().await.map_err(|e| {
                        error!("Failed to set PDP parameter: {:?}", e);
                        Error::Custom("Setting PDP parameter failed")
                    })?;
                    match pdpstatus {
                        Status::OK => {
                            info!("PDP set");
                            let pdpactivationstatus =
                                self.activating_pdp_context().await.map_err(|e| {
                                    error!("Failed to activate PDP context: {:?}", e);
                                    Error::Custom("Activating PDP context failed")
                                })?;
                            match pdpactivationstatus {
                                Status::OK => {
                                    info!("PDP context activated");
                                    let cipstatus = self.check_ip_status().await.map_err(|e| {
                                        error!("Failed to check IP status: {:?}", e);
                                        Error::Custom("Checking IP status failed")
                                    })?;
                                    Ok((cipstatus, pdpactivationstatus))
                                }
                                Status::ModemError(e) => {
                                    error!("Modem responded with an error: {:?}", e);
                                    Err(Error::Custom("Modem error during PDP context activation"))
                                }
                                _ => Err(Error::Custom("Unexpected status")),
                            }
                        }
                        Status::ModemError(e) => {
                            error!("Modem responded with an error: {:?}", e);
                            Err(Error::Custom("Modem error during PDP parameter setting"))
                        }
                        _ => Err(Error::Custom("Unexpected status")),
                    }
                }
                PdpState::Deactivated => {
                    let status = self.attach_network().await.map_err(|e| {
                        error!("Failed to attach network: {:?}", e);
                        Error::Custom("Attaching to network failed")
                    })?;
                    match status {
                        Status::OK => {
                            info!("Attached to the network");
                            let pdpstatus = self.set_pdp_parameter().await.map_err(|e| {
                                error!("Failed to set PDP parameter: {:?}", e);
                                Error::Custom("Setting PDP parameter failed")
                            })?;
                            match pdpstatus {
                                Status::OK => {
                                    info!("PDP set");
                                    let pdpactivationstatus =
                                        self.activating_pdp_context().await.map_err(|e| {
                                            error!("Failed to activate PDP context: {:?}", e);
                                            Error::Custom("Activating PDP context failed")
                                        })?;
                                    match pdpactivationstatus {
                                        Status::OK => {
                                            info!("PDP context activated");
                                            let cipstatus =
                                                self.check_ip_status().await.map_err(|e| {
                                                    error!("Failed to check IP status: {:?}", e);
                                                    Error::Custom("Checking IP status failed")
                                                })?;
                                            Ok((cipstatus, pdpactivationstatus))
                                        }
                                        Status::ModemError(e) => {
                                            error!("Modem responded with an error: {:?}", e);
                                            Err(Error::Custom(
                                                "Modem error during PDP context activation",
                                            ))
                                        }
                                        // Status::CONNECT => {} //
                                        _ => Err(Error::Custom("Unexpected status")),
                                    }
                                }
                                Status::ModemError(e) => {
                                    info!("Modem responded with an error: {:?}", e);
                                    Err(Error::Custom("Modem error during PDP parameter setting"))
                                }
                                _ => Err(Error::Custom("Unexpected status")),
                            }
                        }
                        Status::ModemError(e) => {
                            error!("Modem responded with an error: {:?}", e);
                            Err(Error::Custom("Modem error during network attachment"))
                        }
                        _ => Err(Error::Custom("Unexpected status")),
                    }
                }
            },
            Err(e) => {
                error!("Failed to check attached network: {:?}", e);
                Err(Error::Custom("Checking attached network failed"))
            }
        }
    }

    pub async fn check_ip_status(&mut self) -> Result<CIPSTATUS> {
        // AT+CIPSTATUS
        // self.tosend.set_to_sent(b"AT+CIPSTATUS\r\n");
        // self.send_at(None, Some("check_ip_status"), None).await;
        // let response = self.wait_response(None, None, None).await?;

        let response = {
            let cmd = b"AT+CIPSTATUS\r\n";
            self.send_at(Some(cmd), Some("check_ip_status"), None).await;
            let res_prefix = b"AT+CIPSTATUS\r\n+CIPSTATUS:\r\nSTATE:";
            let res_suffix = b"\r\n\r\nOK\r\n";
            self.need(Some(res_prefix), Some(res_suffix)).await
        };

        match response.1 {
            FoundResponse::M1 => Ok(self
                .modem_res
                .parse_ip_status(&response.0.response.data[..])?),
            _ => Err(Error::Custom("Failed to check IP status")),
        }
    }
    /// Returns the attach network of this [`Modem`].
    /// Attach to the GPRS network, can also use parameter 0 to detach.
    /// Activate GPRS - "AT+CGATT=1"
    pub async fn attach_network(&mut self) -> Result<Status> {
        // self.tosend.set_to_sent(b"AT+CGATT=1\r\n");
        // self.send_at(None, Some("attach_network"), None).await;
        // let response = self.wait_response(None, None, None).await?;

        let response = {
            let cmd = b"AT+CGATT=1\r\n";
            self.send_at(Some(cmd), Some("attach_network"), None).await;
            let res_prefix = b"AT+CGATT=1\r\n";
            let res_suffix = b"\r\n\r\nOK\r\n";
            self.need(Some(res_prefix), Some(res_suffix)).await
        };
        match response.1 {
            FoundResponse::M1 => {
                let status = response.0.response.status;
                Ok(status)
            }
            _ => Err(Error::Custom("failed to attach to network")),
        }
    }
    pub async fn start_ppp(&mut self) -> Result<Status> {
        // self.tosend.set_to_sent(b"AT+CGATT=1\r\n");
        // self.send_at(None, Some("attach_network"), None).await;
        // let response = self.wait_response(None, None, None).await?;

        let response = {
            let cmd = b"ATD*99***1#\r\n";
            self.send_at(Some(cmd), Some("start_ppp"), None).await;
            let res_prefix = b"ATD*99***1#\r\n";
            let res_suffix = b"\r\n\r\nCONNECT\r\n";
            self.need(Some(res_prefix), Some(res_suffix)).await
        };
        match response.1 {
            FoundResponse::M1 => {
                let status = response.0.response.status;
                Ok(status)
            }
            _ => Err(Error::Custom("failed to attach to network")),
        }
    }
    /// Command: AT+CGDCONT=?
    /// Response: +CGDCONT: (1..7), (IP,IPV6,PPP),(0..3),(0..4)
    /// Returns the read pdp context of this [`Modem`].
    /// Input test command for help information.
    pub async fn read_pdp_context(&mut self) -> Result<Status> {
        // self.tosend.set_to_sent(b"AT+CGDCONT=?\r\n");
        // self.send_at(None, Some("read_pdp_context"), None).await;
        // let response = self.wait_response(None, None, None).await?;

        let response = {
            let cmd = b"AT+CGDCONT=?\r\n";
            self.send_at(Some(cmd), Some("read_pdp_context"), None)
                .await;
            let res_prefix = b"AT+CGDCONT=?\r\n\r\n+CGDCONT: ";
            let res_suffix = b"\r\n\r\nOK\r\n";
            self.need(Some(res_prefix), Some(res_suffix)).await
        };
        match response.1 {
            FoundResponse::M1 => {
                let status = response.0.response.status;
                Ok(status)
            }
            _ => Err(Error::Custom("failed to read pdp context")),
        }
    }
    pub async fn set_pdp_parameter(&mut self) -> Result<Status> {
        // "AT+CGDCONT=1,\"IP\",\"CMNET\""
        // "AT+CGDCONT=1,\"IP\",\"internet\""
        // AT+CGDCONT=?
        // self.tosend
        //     .set_to_sent(b"AT+CGDCONT=1,\"IP\",\"CMNET\"\r\n");
        // self.send_at(None, Some("set_pdp_parameter"), None).await;
        // let response = self.wait_response(None, None, None).await?;

        let response = {
            let cmd = b"AT+CGDCONT=1,\"IP\",\"CMNET\"\r\n";
            self.send_at(Some(cmd), Some("set_pdp_parameter"), None)
                .await;
            let res_prefix = b"AT+CGDCONT=1,\"IP\",\"CMNET\"\r\n";
            let res_suffix = b"\r\n\r\nOK\r\n";
            self.need(Some(res_prefix), Some(res_suffix)).await
        };
        match response.1 {
            FoundResponse::M1 => {
                let status = response.0.response.status;
                Ok(status)
            }
            _ => Err(Error::Custom("failed to set pdp parameter")),
        }
    }
    pub async fn activating_pdp_context(&mut self) -> Result<Status> {
        // Activate PDP context - "AT+CGACT=1,1"
        // self.tosend.set_to_sent(b"AT+CGACT=1,1\r\n");
        // self.send_at(None, Some("activating_pdp_context"), None)
        //     .await;
        // let response = self.wait_response(None, None, None).await?;

        let response = {
            let cmd = b"AT+CGACT=1,1\r\n";
            self.send_at(Some(cmd), Some("activating_pdp_context"), None)
                .await;
            let res_prefix = b"AT+CGACT=1,1\r\n";
            let res_suffix = b"\r\n\r\nOK\r\n";
            self.need(Some(res_prefix), Some(res_suffix)).await
        };
        match response.1 {
            FoundResponse::M1 => {
                let status = response.0.response.status;
                Ok(status)
            }
            _ => Err(Error::Custom("Failed to activate PDP context")),
        }
    }
    pub async fn check_attached_network(&mut self) -> Result<PdpState> {
        // get_pdp_context_states
        // Activate GPRS - "AT+CGACT=?"
        // self.tosend.set_to_sent(b"AT+CGACT=?\r\n");
        // self.send_at(None, Some("check_attached_network"), None)
        //     .await;
        // let response = self.wait_response(None, None, None).await?;

        let response = {
            let cmd = b"AT+CGACT=?\r\n";
            self.send_at(Some(cmd), Some("check_attached_network"), None)
                .await;
            let res_prefix = b"AT+CGACT=?\r\n\r\n+CGACT: ";
            let res_suffix = b"\r\n\r\nOK\r\n";
            self.need(Some(res_prefix), Some(res_suffix)).await
        };

        match response.1 {
            FoundResponse::M1 => Ok(self
                .modem_res
                .get_pdp_context_states(&response.0.response.data[..])?),
            _ => Err(Error::Custom("Failed to activate GPRS")),
        }
    }
    /// Returns the get gprs network registration status of this [`Modem`].
    /// GPRS network registration status
    pub async fn get_gprs_network_registration_status(
        &mut self,
    ) -> Result<(RegistrationStatus, Status)> {
        // let response = {
        //     // self.send_at("+CGREG?", None).await?;
        //     self.tosend.set_to_sent(b"AT+CGREG?\r\n");
        //     self.send_at(None, Some("get_gprs_network_registration_status"), None)
        //         .await;
        //     let response = self.wait_response(None, None, None).await?;
        // };
        let response = {
            let cmd = b"AT+CGREG?";
            self.send_at(Some(cmd), Some("gprs_network"), None).await;
            let res_prefix = b"AT+CGREG?\r\n\r\n+CGREG: ";
            let res_suffix = b"\r\n\r\nOK\r\n";
            self.need(Some(res_prefix), Some(res_suffix)).await
        };
        match response.1 {
            FoundResponse::M1 => Ok(self
                .modem_res
                .parse_registration_status(&response.0.response.data[..])?),
            // FoundResponse::M1 => Ok(&response.0),
            _ => Err(Error::Custom("error getting registration status")),
        }
    }
}

impl ModemResponse {
    const fn cipstatus_from_repr(d: &[u8]) -> Option<CIPSTATUS> {
        match d {
            b"IP INITIAL" => Some(CIPSTATUS::IPINITIAL),
            b"CONNECT OK" => Some(CIPSTATUS::CONNECTOK),
            b"IP START" => Some(CIPSTATUS::IPSTART),
            b"IP CONFIG" => Some(CIPSTATUS::IPCONFIG),
            b"IP IND" => Some(CIPSTATUS::IPIND),
            b"IP GPRSACT" => Some(CIPSTATUS::IPGPRSACT),
            b"TCP CONNECTING" => Some(CIPSTATUS::TCPCONNECTING),
            b"IP CLOSE" => Some(CIPSTATUS::IPCLOSE),
            _ => None,
        }
    }
    pub(crate) fn parse_ip_status(&self, cipstatus_response: &[u8]) -> Result<CIPSTATUS> {
        // "AT+CIPSTATUS\r\n+CIPSTATUS:\r\nSTATE:IP INITIAL \r\n\r\nOK\r\n"
        // "+CIPSTATUS:\r\nSTATE:IP INITIAL "
        let prefix = b"+CIPSTATUS:\r\nSTATE:";

        // Check if the response starts with the expected prefix
        if !cipstatus_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse ip status"));
        }

        // Remove the prefix by skipping its length
        let status_content = &cipstatus_response[prefix.len()..];

        // const IPINITIAL: &[u8] = b"IP INITIAL";
        // const CONNECTOK: &[u8] = b"CONNECT OK";
        // const CONNECTOK: &[u8] = b"IP CLOSE";

        match Self::cipstatus_from_repr(status_content) {
            Some(cip) => Ok(cip),
            None => Err(Error::Custom("unknown cipstatus")),
        }
    }

    pub(crate) fn parse_ip_status_old(&self, cipstatus_response: &[u8]) -> Result<u8> {
        // "AT+CIPSTATUS\r\n+CIPSTATUS:\r\nSTATE:IP INITIAL \r\n\r\nOK\r\n"
        // "+CIPSTATUS:\r\nSTATE:IP INITIAL "
        let prefix = b"+CIPSTATUS:\r\nSTATE:";

        // Check if the response starts with the expected prefix
        if !cipstatus_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse ip status"));
        }

        // Remove the prefix by skipping its length
        let status_content = &cipstatus_response[prefix.len()..];

        // Find the position of the `:IP` substring
        match status_content
            .windows(b":IP".len())
            .position(|window| window == b":IP")
        {
            Some(ip_start) => {
                // Check if a number exists before the `:IP` substring
                if status_content[..ip_start]
                    .iter()
                    .rposition(|&b| b.is_ascii_digit())
                    .is_none()
                {
                    return Err(Error::NoNumberExist);
                }
                // Extract the number before the `:IP` substring
                match status_content[..ip_start]
                    .iter()
                    .rposition(|&b| b.is_ascii_digit())
                {
                    Some(number_start) => {
                        let num_str =
                            core::str::from_utf8(&status_content[number_start..number_start + 1])
                                .map_err(|_| Error::ParseError)?;
                        let number: u8 = num_str.parse().map_err(|_| Error::InvalidNumber)?;
                        if number <= 7 {
                            return Ok(number);
                        } else {
                            return Err(Error::InvalidNumber);
                        }
                    }
                    None => return Err(Error::NoNumberExist),
                }
            }
            None => return Err(Error::NoNumberExist),
        }
    }
}
