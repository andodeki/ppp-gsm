#[derive(Debug)]
pub enum Error {
    Custom(&'static str),
    ParseError,
    BorrowError,
    InvalidState,
    InvalidNumber,
    NoNumberExist,
    InitTimeout,
    Timeout,
    UartError(),
}

pub type Result<T> = core::result::Result<T, Error>;
