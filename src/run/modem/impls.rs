use embedded_io_async::{BufRead, ErrorType, Read, Write};

use super::module::UartWrapper;

impl<'a> ErrorType for UartWrapper<'a> {
    type Error = hal::uart::Error;
}

impl<'a> Read for UartWrapper<'a> {
    async fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::Error> {
        let available = self.write_pos - self.read_pos;
        let to_copy = available.min(buf.len());
        buf[..to_copy].copy_from_slice(&self.buffer[self.read_pos..self.read_pos + to_copy]);
        self.read_pos += to_copy;

        if to_copy == 0 {
            // Buffer is empty, read directly from UART
            self.uart1.rx.read(buf).await
        } else {
            Ok(to_copy)
        }
    }
}

impl<'a> Write for UartWrapper<'a> {
    async fn write(&mut self, buf: &[u8]) -> Result<usize, Self::Error> {
        self.uart1.tx.write(buf).await
    }
}

impl<'a> BufRead for UartWrapper<'a> {
    async fn fill_buf(&mut self) -> Result<&[u8], Self::Error> {
        if self.read_pos == self.write_pos {
            // Buffer is empty, fill it
            self.read_pos = 0;
            self.write_pos = self.uart1.rx.read(&mut self.buffer).await?;
        }
        Ok(&self.buffer[self.read_pos..self.write_pos])
    }

    fn consume(&mut self, amt: usize) {
        self.read_pos = (self.read_pos + amt).min(self.write_pos);
    }
}
