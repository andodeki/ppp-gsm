use crate::singleton;

use super::{
    errors::{Error, Result},
    parse::parse::{ModemResponse, Status, ToSend},
};
use alloc::{
    // rc::Rc,
    string::ToString,
    vec::Vec,
};
use embassy_sync::{blocking_mutex::raw::NoopRawMutex, signal::Signal};
use embassy_time::{with_timeout, Duration, Instant, Timer};
use embedded_io_async::Write;
use hal::{
    clock::Clocks,
    gpio::GpioPin,
    peripherals::{UART0, UART1},
    uart::{
        config::{Config, DataBits, Parity, StopBits},
        ClockSource, Uart, UartRx, UartTx,
    },
    Async,
};
use log::{error, info, warn};
// uart::{
//     config::{Config as UartConfig, DataBits, Parity, StopBits},
//     ClockSource, TxRxPins, Uart, UartTx,
// },
// use static_cell::StaticCell;
pub struct SharedUart1<'a> {
    pub tx: UartTx<'a, UART1, Async>,
    pub rx: UartRx<'a, UART1, Async>,
}

pub struct UartWrapper<'a> {
    pub uart1: SharedUart1<'a>,
    // pub uart1: Uart<'a, UART1, Async>,
    pub signal: &'a Signal<NoopRawMutex, (usize, Vec<u8>)>,
    pub buffer: [u8; 256], // Adjust size as needed
    pub read_pos: usize,
    pub write_pos: usize,
    pub modem_res: ModemResponse,
    pub tosend: ToSend,
}
impl<'a> UartWrapper<'a> {
    pub async fn init_with_retry(&mut self) -> Result<()> {
        for attempt in 1..=3 {
            info!("Init attempt {}", attempt);
            match with_timeout(Duration::from_secs(10), self.init(None)).await {
                Ok(result) => return result,
                Err(_) => warn!("Init attempt {} timed out", attempt),
            }
        }
        Err(Error::InitTimeout)
    }
    // async fn wait_for_response(&mut self, timeout: Duration) -> Result<Vec<u8>> {
    //     let mut buffer = Vec::new();
    //     let deadline = Timer::after(timeout);

    //     // let mut modem_guard = modem.lock().await;

    //     loop {
    //         match embassy_futures::select::select(self.uart1.read(), deadline).await {
    //             embassy_futures::select::Either::First(Ok(byte)) => {
    //                 buffer.push(byte);
    //                 if buffer.ends_with(b"\r\nOK\r\n") {
    //                     return Ok(buffer);
    //                 }
    //             }
    //             embassy_futures::select::Either::First(Err(e)) => return Err(Error::UartError(e)),
    //             embassy_futures::select::Either::Second(_) => return Err(Error::Timeout),
    //         }
    //     }
    // }
    pub async fn init(&mut self, timeout_ms: Option<u64>) -> Result<()> {
        let timeout = timeout_ms.unwrap_or(30000);
        let start_time = Instant::now();
        loop {
            info!("Modem: init function!");
            // Send a simple AT command
            let (_, found_response) = {
                let cmd = b"AT\r\n";
                self.send_at(Some(cmd), Some("init"), None).await;
                let isok_res_prefix = b"AT\r\n\r\nOK\r\n";
                self.need(Some(isok_res_prefix), None).await
            };
            if start_time.elapsed().as_millis() > timeout {
                info!("timeout initializing modem");
            }
            match found_response {
                FoundResponse::M1 => break,
                _ => Timer::after(Duration::from_millis(1000)).await,
            };
        }
        esp_println::println!("\nsuccessfully established communication with the modem");
        Ok(())
    }
    /// Check that the cellular module is alive.
    ///
    /// See if the cellular module is responding at the AT interface by poking
    /// it with "AT" up to `attempts` times, waiting 1 second for an "OK"
    /// response each time
    pub async fn is_alive(&mut self, attempts: u8) -> Result<Status> {
        for i in 0..attempts {
            let (res, found_response) = {
                let cmd = b"AT\r\n";
                self.send_at(
                    Some(cmd),
                    Some(("is_alive: ".to_string() + &i.to_string()).as_str()),
                    None,
                )
                .await;
                let isok_res_prefix = b"AT\r\n\r\nOK\r\n";
                self.need(Some(isok_res_prefix), None).await
            };
            match found_response {
                FoundResponse::M1 => {
                    return Ok(res.response.status);
                }
                FoundResponse::M2 => {
                    return Ok(res.response.status);
                }
                _ => {
                    continue;
                }
            }
        }

        Err(Error::Custom("Modem not responding in is_alive"))
    }
    pub async fn check_network_registration(&mut self) -> Result<u8> {
        let response = {
            let cmd = b"AT+CREG?\r\n";
            self.send_at(Some(cmd), Some("check_network_registration"), None)
                .await;
            let res_prefix = b"AT+CREG?\r\n\r\n+CREG: ";
            let res_suffix = b"\r\n\r\nOK\r\n";
            self.need(Some(res_prefix), Some(res_suffix)).await
        };
        match response.1 {
            FoundResponse::M1 => Ok(self
                .modem_res
                .parse_network_registaration(&response.0.response.data[..])?),
            _ => Err(Error::Custom("error getting subscriber number")),
        }
    }

    pub async fn need(
        &mut self,
        gps_prefixparam: Option<&[u8]>,
        gps_suffixparam: Option<&[u8]>,
    ) -> (ModemResponse, FoundResponse) {
        let gps_prefix = gps_prefixparam.unwrap_or(AT_CMD);
        let gps_suffix = gps_suffixparam.unwrap_or(AT_CMD);
        let mut modem_response = ModemResponse::new();
        let mut found_response = FoundResponse::None;
        let mut received_bytes: Vec<u8> = Vec::new();

        loop {
            let (_len, line_bytes) = self.signal.wait().await;
            received_bytes.extend_from_slice(&line_bytes);
            self.signal.reset();
            info!("looping……");

            if gps_prefixparam.is_some() && gps_suffixparam.is_some() {
                if let Some((prefix_pos, suffix_pos)) =
                    find_prefix_suffix(&received_bytes, gps_prefix, gps_suffix)
                {
                    let extracted = &received_bytes[prefix_pos..suffix_pos + gps_suffix.len()];
                    modem_response = self
                        .modem_res
                        .parse0(&extracted, Some(self.tosend.get_to_sent()));
                    found_response = FoundResponse::M1;
                    break;
                }
            } else if let Some(pos) = find_sequence(&received_bytes, gps_prefix) {
                let extracted = &received_bytes[pos..pos + gps_prefix.len()];
                modem_response = self
                    .modem_res
                    .parse0(&extracted, Some(self.tosend.get_to_sent()));
                found_response = FoundResponse::M1;
                break;
            }

            if find_sequence(&received_bytes, b"ERROR").is_some() {
                modem_response = self
                    .modem_res
                    .parse0(&received_bytes, Some(self.tosend.get_to_sent()));
                found_response = FoundResponse::M2;
                break;
            }
        }
        (modem_response, found_response)
    }
}
const AT_CMD: &[u8] = b"AT\r\n";
// const AT_OK_RESPONSE: &[u8] = b"AT\r\n\r\nOK\r\n";
// const AT_CREG: &[u8] = b"AT+CREG?\r\n";
// const CREG_RESPONSE_PREFIX: &[u8] = b"AT+CREG?\r\n\r\n+CREG: ";
// const RESPONSE_SUFFIX: &[u8] = b"\r\n\r\nOK\r\n";

fn find_prefix_suffix(buffer: &[u8], prefix: &[u8], suffix: &[u8]) -> Option<(usize, usize)> {
    if let Some(prefix_pos) = buffer
        .windows(prefix.len())
        .position(|window| window == prefix)
    {
        let after_prefix_pos = prefix_pos + prefix.len();
        if let Some(suffix_pos) = buffer[after_prefix_pos..]
            .windows(suffix.len())
            .position(|window| window == suffix)
        {
            return Some((prefix_pos, after_prefix_pos + suffix_pos));
        }
    }
    None
}

fn find_sequence(buffer: &[u8], sequence: &[u8]) -> Option<usize> {
    buffer
        .windows(sequence.len())
        .position(|window| window == sequence)
}
#[derive(Debug)]
pub enum FoundResponse {
    None,
    M1,
    M2,
}
#[derive(Debug, PartialEq)]
#[repr(u8)]
pub enum RegistrationStatus {
    NotRegistered = 0,
    OkHome = 1,
    Searching = 2,
    Denied = 3,
    Unknown = 4,
    OkRoaming = 5,
}
#[derive(Debug, PartialEq)]
#[repr(u8)]
pub enum PreferredMode {
    Automatic = 2,
    CDMAOnly = 9,
    EVDOOnly = 10,
    GSMOnly = 13,
    WCDMAOnly = 14,
    GSMPlusWCDMAOnly = 19,
    CDMAPlusEVDOOnly = 22,
    LTEOnly = 37,
    GSMPlusWCDMAPlusLTEOnly = 39,
    AnyButLTE = 48,
    GSMPlusLTEOnly = 51,
    WCDMAPlusLTEOnly = 54,
    TDCDMAOnly = 59,
    GSMPlusTDSCDMAOnly = 60,
    GSMPlusWCDMAPlusTDSCDMAOnly = 63,
    CDMAPlusEVDOPlusGSMPlusWCDMAPlusTDSCDMAOnly = 67,
}
#[derive(Debug, PartialEq)]
#[repr(u16)]
pub enum ModemErrors {
    OPERATIONNOTALLOWED = 3,
    EXENOTSUPPORT = 49,
    EXEFAIL = 50,
    PARAMINVALID = 53,
    INVALIDCOMMANDLINE = 58,
    OPERNOTSUPP = 303,
    INVALIDTXTPARAM = 305,
    INVALIDMEMINDEX = 321,
    UNKNOWNERROR = 500,
}

#[derive(Debug, PartialEq)]
pub enum SIMStatus {
    UNKNOWN,
    READY,
    SIMPIN,
    SIMPUK,
    PHSIMPIN,
    SIMPIN2,
    SIMPUK2,
    PHNETPIN,
}

pub struct SignalQuality {
    pub rssi: u8,
    pub ber: u8,
}

impl SignalQuality {
    pub fn rssi(&self) -> Option<i8> {
        match self.rssi {
            0 => Some(-115),
            1 => Some(-111),
            2..=32 => Some(-110 + 2 * (self.rssi - 2) as i8),
            _ => None,
        }
    }
}
