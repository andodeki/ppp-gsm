use core::fmt;

use alloc::vec::Vec;
use log::info;

use crate::run::modem::{
    // apn::Apn,
    errors::{Error, Result},
    module::{ModemErrors, PreferredMode, RegistrationStatus, SIMStatus, SignalQuality},
};

pub struct ToSend {
    pub to_send: Vec<u8>,
    pub len: usize,
    pub timeout_ms: Option<u64>,
}

impl ToSend {
    pub fn new() -> Self {
        Self {
            to_send: Vec::new(),
            len: 0,
            timeout_ms: Some(0),
        }
    }
    pub fn at_tosend(&mut self, _cmd: &[u8]) -> Self {
        let cmd_to_send = self.get_to_sent();
        Self {
            to_send: cmd_to_send.to_vec(),
            len: cmd_to_send.len(),
            timeout_ms: Some(10000),
        }
    }
    pub fn empty_tosend(&mut self) -> Self {
        Self {
            to_send: Vec::new(),
            len: 0,
            timeout_ms: Some(0),
        }
    }
    pub fn at_terminal_cmd(&mut self, cmd: &[u8]) -> Self {
        // let mut extended_cmd = cmd.to_vec();
        // extended_cmd.push(10);
        let cleaned_cmd = clean_array::remove_chars_and_add_newline(&cmd[..]);

        Self {
            to_send: cleaned_cmd,
            len: cmd.len() + 1,
            timeout_ms: Some(10000),
        }
    }

    pub fn clear(&mut self) {
        self.to_send = Vec::new();
        self.len = 0;
    }
    pub fn get_to_sent(&mut self) -> &[u8] {
        let at = [65, 84];
        if self.to_send[..]
            .windows(at.len())
            .any(|window| window == at)
        {
            &self.to_send[..]
            // found_response = FoundResponse::M1;
        } else if self.to_send[..]
            .windows([26].len())
            .any(|window| window == [26])
        {
            &self.to_send[..]
            // found_response = FoundResponse::M1;
        } else {
            esp_println::println!("\nAT sent does not have the PREFIX [AT]");
            &self.to_send[..]
        }
    }
    pub fn set_to_sent(&mut self, cmd: &[u8]) {
        let cleaned_cmd = clean_array::remove_chars_and_add_newline(&cmd[..]);
        self.to_send = cleaned_cmd;
    }
    pub fn cmd_t(&mut self, cmd: &[u8]) -> Vec<u8> {
        let cleaned_cmd = clean_array::remove_chars_and_add_newline(&cmd[..]);
        cleaned_cmd
    }
    pub fn get_timeout(&mut self) -> Option<u64> {
        self.timeout_ms
    }
}

pub enum Status {
    OK,
    CONNECT,
    ModemError(Option<ModemErrors>),
}

pub enum PdpState {
    Deactivated = 0,
    Activated = 1,
}

pub struct ResponseData {
    pub data: Vec<u8>,
    pub status: Status,
    // error: Option<[u8; 8]>,
}

impl ResponseData {
    pub fn new() -> Self {
        ResponseData {
            data: Vec::new(),
            status: Status::ModemError(None),
        }
    }

    pub fn set_data(&mut self, bytes: &[u8]) {
        // let length = bytes.len();
        // self.data[..length].copy_from_slice(&bytes[..length]);
        self.data.extend_from_slice(&bytes[..]);
    }

    pub fn set_status(&mut self, bytes: &[u8]) {
        if bytes.contains(&79) && bytes.contains(&75) {
            self.status = Status::OK;
        } else {
        }
    }
    pub fn set_error_from_modem(&mut self, e: ModemErrors) {
        self.status = Status::ModemError(Some(e));
    }
}

impl core::fmt::Debug for ResponseData {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let cleaned = clean_array::remove_invalid_utf8(&self.data[..]);
        let data_str = core::str::from_utf8(&cleaned).unwrap_or("<invalid utf-8>");
        // let status_str = core::str::from_utf8(&self.status).unwrap_or("<invalid utf-8>");

        f.debug_struct("ResponseData")
            .field("data", &data_str)
            .field("status", &self.status)
            .finish()
        // f.debug_struct("ResponseData")
        //     .field("data", &self.data)
        //     .field("status", &self.status)
        //     .field("error", &error_str)
        //     .finish()
    }
}

pub struct ModemResponse {
    pub bytesresponse: Vec<u8>,
    pub command_sent: Vec<u8>,
    pub response: ResponseData,
}

pub struct DateTime {
    pub datetime: Vec<u8>,
    pub date: Vec<u8>,
    pub time: Vec<u8>,
}
impl ModemResponse {
    pub fn new() -> Self {
        ModemResponse {
            bytesresponse: Vec::new(),
            command_sent: Vec::new(),
            response: ResponseData::new(),
        }
    }

    pub fn set_command_sent(&mut self, command: &[u8]) {
        // let length = command.len();
        // self.command_sent[..length].copy_from_slice(&command[..length]);
        self.command_sent.extend_from_slice(&command[..]);
    }
    pub fn set_bytes_response(&mut self, command: &[u8]) {
        self.bytesresponse.extend_from_slice(&command[..]);
    }
    pub fn clear(&mut self) {
        self.command_sent = Vec::new();
        self.response = ResponseData {
            data: Vec::new(),
            status: Status::ModemError(None),
        }
    }

    pub fn parse0(&mut self, received_bytes: &[u8], separator: Option<&[u8]>) -> Self {
        // if received_bytes
        //     .windows([26].len())
        //     .any(|window| window == [26])
        //     && self.bytesresponse.is_empty()
        // {
        //     self.set_bytes_response(received_bytes);
        // }
        if self.bytesresponse.is_empty() {
            if received_bytes
                .windows([26].len())
                .any(|window| window == [26])
                && self.bytesresponse.is_empty()
            {
                self.set_bytes_response(received_bytes);
            }
        } else {
            self.bytesresponse.clear()
        }
        let mut modem_response = ModemResponse::new();

        let (nested_chunks, _final_chunk) = split_nested(&received_bytes, separator);
        // if received_bytes
        //     .windows([26].len())
        //     .any(|window| window == [26])
        // {
        //     esp_println::println!("\nHere, nested_chunks: {:?}", nested_chunks.len());
        //     esp_println::println!("\nHere, nested_chunks: {:?}", nested_chunks);
        // }
        for (i, v) in nested_chunks.iter().enumerate() {
            if nested_chunks.len() == 4
                && received_bytes
                    .windows([26].len())
                    .any(|window| window == [26])
            {
                match i {
                    0 => modem_response.set_command_sent(v),
                    1 => {
                        // debug_println!("Here: {:?}", v);
                        modem_response.set_command_sent(v);
                        modem_response.bytesresponse.clear();
                    }
                    2 => modem_response.response.set_data(v),
                    3 => modem_response.response.set_status(v),
                    _ => {
                        esp_println::println!("\nOther element at index {}: {:?}", i, v);
                        // Perform some action for other elements
                    }
                }
            } else if nested_chunks.len() == 3 {
                match i {
                    0 => modem_response.set_command_sent(v),
                    1 => {
                        // debug_println!("Here: {:?}", v);
                        modem_response.response.set_data(v)
                    }
                    2 => modem_response.response.set_status(v),
                    _ => {
                        esp_println::println!("\nOther element at index {}: {:?}", i, v);
                        // Perform some action for other elements
                    }
                }
            } else if nested_chunks.len() == 2 {
                //OK or CONNECT without data
                match i {
                    0 => modem_response.set_command_sent(v),
                    1 => {
                        let okcrlf = [79, 75, 13, 10];
                        let ee = [69, 82, 82, 79, 82];
                        // let ctrlz = [26];
                        if v.windows(okcrlf.len()).any(|window| window == okcrlf) {
                            // debug_println!("Here: {:?}", v);
                            modem_response.response.set_status(v)
                        } else if v.windows(ee.len()).any(|window| window == ee) {
                            // debug_println!("ModemError: {:?}", v);
                            match self.parse_error(v) {
                                Ok(e) => {
                                    // Successfully parsed the error
                                    // Use `e` as needed
                                    esp_println::println!("\nParsed error: {:?}", e);
                                    modem_response.response.set_error_from_modem(e)
                                }
                                Err(_) => {
                                    // Handle the error case, possibly log it or take corrective action
                                    esp_println::println!("\nFailed to parse error");
                                }
                            }
                        } else if v.windows([26].len()).any(|window| window == [26]) {
                            // debug_println!("Here: {:?}", v);
                            modem_response.response.set_data(v)
                        } else {
                            info!("OK or Error not found{:?}", v);
                        }
                    }
                    _ => {
                        info!("Other element at index {}: {:?}", i, v);
                    }
                }
            } else if nested_chunks.len() == 1 {
                match i {
                    0 => {
                        let okcrlf = [79, 75, 13, 10];
                        let ee = [69, 82, 82, 79, 82];
                        // debug_println!("Here: {:?}", v);
                        if v.windows(ee.len()).any(|window| window == ee) {
                            match self.parse_error(v) {
                                Ok(e) => {
                                    esp_println::println!("\nParsed error: {:?}", e);
                                    modem_response.response.set_error_from_modem(e)
                                }
                                Err(_) => {
                                    esp_println::println!("\nFailed to parse error");
                                }
                            }
                        } else {
                            modem_response.response.set_data(v)
                        }
                    }
                    1 => modem_response.response.set_status(v),
                    _ => {
                        info!("Other element at index {}: {:?}", i, v);
                    }
                }
                // debug_println!("NOT COVERED");
            } else {
                info!("NOT COVERED");
            }
        }
        modem_response
    }

    const fn modem_error_from_repr(d: u16) -> Option<ModemErrors> {
        match d {
            3 => Some(ModemErrors::OPERATIONNOTALLOWED),
            49 => Some(ModemErrors::EXENOTSUPPORT),
            50 => Some(ModemErrors::EXEFAIL),
            53 => Some(ModemErrors::PARAMINVALID),
            58 => Some(ModemErrors::INVALIDCOMMANDLINE),
            303 => Some(ModemErrors::OPERNOTSUPP),
            305 => Some(ModemErrors::INVALIDTXTPARAM),
            321 => Some(ModemErrors::INVALIDMEMINDEX),
            500 => Some(ModemErrors::UNKNOWNERROR),
            _ => None,
        }
    }

    pub fn parse_error(&mut self, error_response: &[u8]) -> Result<ModemErrors> {
        // Convert the prefix into a byte array
        // Define the possible prefixes
        let cme_prefix = b"+CME ERROR: ";
        let cms_prefix = b"+CMS ERROR: ";

        let data = if error_response.starts_with(cme_prefix) {
            // If the response starts with the CME prefix, strip it
            &error_response[cme_prefix.len()..]
        } else if error_response.starts_with(cms_prefix) {
            // If the response starts with the CMS prefix, strip it
            &error_response[cms_prefix.len()..]
        } else {
            // If the response doesn't start with any known prefix, return an error
            return Err(Error::Custom("no matches in parse error"));
        };

        // Parse the raw_rssi part as a u8
        let number = core::str::from_utf8(data)
            .map_err(|_| Error::ParseError)?
            .trim()
            .parse::<u16>()
            .map_err(|_| Error::ParseError)?;
        // Convert the modem_error_from_repr to a ModemErrors enum
        match Self::modem_error_from_repr(number) {
            Some(rs) => Ok(rs),
            None => Err(Error::Custom("unknown code")),
        }
    }
    const fn registration_status_from_repr(d: u8) -> Option<RegistrationStatus> {
        match d {
            0 => Some(RegistrationStatus::NotRegistered),
            1 => Some(RegistrationStatus::OkHome),
            2 => Some(RegistrationStatus::Searching),
            3 => Some(RegistrationStatus::Denied),
            4 => Some(RegistrationStatus::Unknown),
            5 => Some(RegistrationStatus::OkRoaming),
            _ => None,
        }
    }
    const fn preferred_mode_from_repr(d: &[u8]) -> Option<PreferredMode> {
        match d {
            b"2" => Some(PreferredMode::Automatic),
            b"9" => Some(PreferredMode::CDMAOnly),
            b"10" => Some(PreferredMode::EVDOOnly),
            b"13" => Some(PreferredMode::GSMOnly),
            b"14" => Some(PreferredMode::WCDMAOnly),
            b"19" => Some(PreferredMode::GSMPlusWCDMAOnly),
            b"22" => Some(PreferredMode::CDMAPlusEVDOOnly),
            b"37" => Some(PreferredMode::LTEOnly),
            b"39" => Some(PreferredMode::GSMPlusWCDMAPlusLTEOnly),
            b"48" => Some(PreferredMode::AnyButLTE),
            b"51" => Some(PreferredMode::GSMPlusLTEOnly),
            b"54" => Some(PreferredMode::WCDMAPlusLTEOnly),
            b"59" => Some(PreferredMode::TDCDMAOnly),
            b"60" => Some(PreferredMode::GSMPlusTDSCDMAOnly),
            b"63" => Some(PreferredMode::GSMPlusWCDMAPlusTDSCDMAOnly),
            b"67" => Some(PreferredMode::CDMAPlusEVDOPlusGSMPlusWCDMAPlusTDSCDMAOnly),
            _ => None,
        }
    }
    const fn sim_status_from_repr(d: &[u8]) -> Option<SIMStatus> {
        match d {
            b"UNKNOWN" => Some(SIMStatus::UNKNOWN),
            b"READY" => Some(SIMStatus::READY),
            b"SIMPIN" => Some(SIMStatus::SIMPIN),
            b"SIMPUK" => Some(SIMStatus::SIMPUK),
            b"PHSIMPIN" => Some(SIMStatus::PHSIMPIN),
            b"SIMPIN2" => Some(SIMStatus::SIMPIN2),
            b"SIMPUK2" => Some(SIMStatus::SIMPUK2),
            b"PHNETPIN" => Some(SIMStatus::PHNETPIN),
            _ => None,
        }
    }

    pub fn parse_realtime_clock(&self, cclk_response: &[u8]) -> Result<DateTime> {
        // Modem Response String +CCLK: “24/07/01,19:29:31+03”
        let prefix = b"+CCLK:";
        // Check if the response starts with the expected prefix
        if !cclk_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse realtime clock"));
        }
        // Remove the prefix by skipping its length
        let datetime = &cclk_response[prefix.len()..].to_vec();

        // Find the first comma in the remaining data
        let first_comma_idx = datetime
            .iter()
            .position(|&x| x == b',')
            .ok_or(Error::Custom("invalid format"))?;

        // Split the remaining data at the first comma position
        let (date, second_part) = datetime.split_at(first_comma_idx);

        // Remove the first comma from the second part
        let time = &second_part[1..].to_vec();

        Ok(DateTime {
            datetime: datetime.to_vec(),
            date: date.to_vec(),
            time: time.to_vec(),
        })
    }

    pub fn parse_operator_selection(&self, cops_response: &[u8]) -> Result<Vec<u8>> {
        // Modem Response String +COPS: 0,2,”63902”

        let prefix = b"+COPS: ";
        // Check if the response starts with the expected prefix
        if !cops_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse operator selection"));
        }
        // Remove the prefix by skipping its length
        let data = &cops_response[prefix.len()..];

        // Find the second comma in the data
        let second_comma_idx = data
            .iter()
            .enumerate()
            .filter(|(_, &x)| x == b',')
            .nth(1)
            .map(|(idx, _)| idx)
            .ok_or(Error::Custom("second comma not found"))?;

        // Split the remaining data at the first comma position
        let (_mode_format, raw_operator) = data.split_at(second_comma_idx);

        // Remove the first comma from the second part
        let operator = &raw_operator[1..];
        Ok(operator.to_vec())
    }

    pub fn parse_signal_quality(&self, csq_response: &[u8]) -> Result<SignalQuality> {
        // Modem Response String +CSQ: 20,99
        //
        let prefix = b"+CSQ: ";
        // Check if the response starts with the expected prefix
        if !csq_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse signal quality"));
        }
        // Remove the prefix by skipping its length
        let data = &csq_response[prefix.len()..];

        // Find the first comma in the remaining data
        let first_comma_idx = data
            .iter()
            .position(|&x| x == b',')
            .ok_or(Error::Custom("invalid format"))?;

        // Split the remaining data at the first comma position
        let (raw_rssi, raw_ber) = data.split_at(first_comma_idx);

        // Remove the first comma from the second part
        let flber = &raw_ber[1..];

        // Parse the raw_rssi part as a u8
        let rssi = core::str::from_utf8(raw_rssi)
            .map_err(|_| Error::ParseError)?
            .trim()
            .parse::<u8>()
            .map_err(|_| Error::ParseError)?;
        // Parse the second part as a u8
        let ber = core::str::from_utf8(flber)
            .map_err(|_| Error::ParseError)?
            .trim()
            .parse::<u8>()
            .map_err(|_| Error::ParseError)?;

        Ok(SignalQuality { rssi, ber })
    }
    pub fn parse_sim_status(&self, cpin_response: &[u8]) -> Result<SIMStatus> {
        // Modem Response String +CPIN:READY
        let prefix = b"+CPIN:";
        // Check if the response starts with the expected prefix
        if !cpin_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse sim status"));
        }
        // Remove the prefix by skipping its length
        let data = &cpin_response[prefix.len()..];

        // // Parse the first part as a u8
        // let sim_status = core::str::from_utf8(data)
        //     .map_err(|_| Error::ParseError)?
        //     .trim()
        //     .parse::<&str>()
        //     .map_err(|_| Error::ParseError)?;
        // Convert the network_reg_status to a RegistrationStatus enum
        match Self::sim_status_from_repr(data) {
            Some(rs) => Ok(rs),
            None => Err(Error::Custom("unknown code")),
        }
    }
    pub fn parse_subscriber_number(&self, cnum_response: &[u8]) -> Result<Vec<u8>> {
        // Modem Response String +CNUM: “11”,”+254114904097”,145
        let prefix = b"+CNUM: ";
        // Check if the response starts with the expected prefix
        if !cnum_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse subscriber number"));
        }
        // Remove the prefix by skipping its length
        let data = &cnum_response[prefix.len()..];

        // Find the second comma in the data
        let second_comma_idx = data
            .iter()
            .enumerate()
            .filter(|(_, &x)| x == b',')
            .nth(1)
            .map(|(idx, _)| idx)
            .ok_or(Error::Custom("second comma not found"))?;

        // Split the remaining data at the first comma position
        let (alpha_number, _ty_pe) = data.split_at(second_comma_idx);

        // Find the first comma in the alpha_number data
        let first_comma_idx = alpha_number
            .iter()
            .position(|&x| x == b',')
            .ok_or(Error::Custom("invalid format"))?;

        // Split the remaining data at the first comma position
        let (_alpha, number) = data.split_at(first_comma_idx);
        Ok(number.to_vec())
    }

    pub fn parse_preferred_mode(&self, cpol_response: &[u8]) -> Result<PreferredMode> {
        // Modem Response String +CPOL: (1-99),2
        let prefix = b"+CPOL:";

        // debug_println!("parse_registration_status: {:?}", cgreg_response);

        // Check if the response starts with the expected prefix
        if !cpol_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse preferred mode"));
        }

        // Remove the prefix by skipping its length
        let data = &cpol_response[prefix.len()..];

        // Find the first comma in the remaining data
        let first_comma_idx = data
            .iter()
            .position(|&x| x == b',')
            .ok_or(Error::Custom("invalid format"))?;

        // Split the remaining data at the first comma position
        let (_first_part, second_part) = data.split_at(first_comma_idx);

        // Remove the first comma from the second part
        let second_part = &second_part[1..];

        match Self::preferred_mode_from_repr(second_part) {
            Some(rs) => Ok(rs),
            None => Err(Error::Custom("unknown code")),
        }
    }

    pub(crate) fn parse_activate_gprs_respose(&self, cgatt_response: &[u8]) -> Result<u8> {
        // Modem Response String +CGATT:1
        //
        let prefix = b"+CGATT: ";
        // Check if the response starts with the expected prefix
        if !cgatt_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse activate gprs respose"));
        }
        // Remove the prefix by skipping its length
        let data = &cgatt_response[prefix.len()..];

        // Parse the raw_rssi part as a u8
        let number = core::str::from_utf8(data)
            .map_err(|_| Error::ParseError)?
            .trim()
            .parse::<u8>()
            .map_err(|_| Error::ParseError)?;
        Ok(number)
    }

    pub(crate) fn parse_mqtt_subscriber(&self, mqqt_publish_response: &[u8]) -> Result<u8> {
        // Modem Response String +MQTTPUBLISH:1,app, 10,1234567890
        //
        let prefix = b"+MQTTPUBLISH:";
        // Check if the response starts with the expected prefix
        if !mqqt_publish_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse mqtt subscriber"));
        }
        // Remove the prefix by skipping its length
        let data = &mqqt_publish_response[prefix.len()..];

        // Find the first comma in the remaining data
        let first_comma_idx = data
            .iter()
            .position(|&x| x == b',')
            .ok_or(Error::Custom("invalid format"))?;

        // Split the remaining data at the first comma position
        let (first_part, _second_part) = data.split_at(first_comma_idx);

        // Parse the raw_rssi part as a u8
        let number = core::str::from_utf8(first_part)
            .map_err(|_| Error::ParseError)?
            .trim()
            .parse::<u8>()
            .map_err(|_| Error::ParseError)?;
        Ok(number)
    }

    pub(crate) fn parse_mqtt_publish(&self, mqqt_publish_response: &[u8]) -> Result<u8> {
        // Modem Response String +MQTTPUBLISH: 1,test, 6,1234563
        //
        let prefix = b"+MQTTPUBLISH: ";
        // Check if the response starts with the expected prefix
        if !mqqt_publish_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse mqtt publish"));
        }
        // Remove the prefix by skipping its length
        let data = &mqqt_publish_response[prefix.len()..];

        // Find the first comma in the remaining data
        let first_comma_idx = data
            .iter()
            .position(|&x| x == b',')
            .ok_or(Error::Custom("invalid format"))?;

        // Split the remaining data at the first comma position
        let (first_part, _second_part) = data.split_at(first_comma_idx);

        // Parse the raw_rssi part as a u8
        let number = core::str::from_utf8(first_part)
            .map_err(|_| Error::ParseError)?
            .trim()
            .parse::<u8>()
            .map_err(|_| Error::ParseError)?;
        Ok(number)
    }
    pub fn parse_registration_status(
        &mut self,
        cgreg_response: &[u8],
    ) -> Result<(RegistrationStatus, Status)> {
        // Convert the prefix into a byte array
        // +CGREG: 2,1,"10DC","0D2B"
        let prefix = b"+CGREG:";

        // debug_println!("parse_registration_status: {:?}", cgreg_response);

        // Check if the response starts with the expected prefix
        if !cgreg_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse registration status"));
        }

        // Remove the prefix by skipping its length
        let data = &cgreg_response[prefix.len()..];

        // Find the first comma in the remaining data
        let first_comma_idx = data
            .iter()
            .position(|&x| x == b',')
            .ok_or(Error::Custom("invalid format"))?;

        // Split the remaining data at the first comma position
        let (first_part, second_part) = data.split_at(first_comma_idx);

        // Remove the first comma from the second part
        let second_part = &second_part[1..];

        // Parse the first part as a u8
        let _network_reg_mode = core::str::from_utf8(first_part)
            .map_err(|_| Error::ParseError)?
            .trim()
            .parse::<u8>()
            .map_err(|_| Error::ParseError)?;

        // Parse the second part as a u8
        let network_reg_status = core::str::from_utf8(second_part)
            .map_err(|_| Error::ParseError)?
            .trim()
            .parse::<u8>()
            .map_err(|_| Error::ParseError)?;

        // Convert the network_reg_status to a RegistrationStatus enum
        match Self::registration_status_from_repr(network_reg_status) {
            Some(rs) => Ok((rs, Status::OK)),
            None => Err(Error::Custom("unknown code")),
        }
    }

    pub(crate) fn parse_network_registaration(&self, network_status_response: &[u8]) -> Result<u8> {
        // +CREG: 1,1
        let prefix = b"+CREG: ";
        // Check if the response starts with the expected prefix
        if !network_status_response.starts_with(prefix) {
            return Err(Error::Custom("no matches in parse network registaration"));
        }
        // Remove the prefix by skipping its length
        let data = &network_status_response[prefix.len()..];

        // Find the first comma in the remaining data
        let first_comma_idx = data
            .iter()
            .position(|&x| x == b',')
            .ok_or(Error::Custom("invalid format"))?;

        // Split the remaining data at the first comma position
        let (first_part, second_part) = data.split_at(first_comma_idx);
        // Remove the first comma from the second part
        let part2 = &second_part[1..];

        // Parse the raw_rssi part as a u8
        let n = core::str::from_utf8(first_part)
            .map_err(|_| Error::ParseError)?
            .trim()
            .parse::<u8>()
            .map_err(|_| Error::ParseError)?;
        // Parse the second part as a u8
        let _stat = core::str::from_utf8(part2)
            .map_err(|_| Error::ParseError)?
            .trim()
            .parse::<u8>()
            .map_err(|_| Error::ParseError)?;
        Ok(n)
    }

    pub(crate) fn get_pdp_context_states(&self, response: &[u8]) -> Result<PdpState> {
        // Response +CGACT: (0,1)
        let prefix = b"+CGACT: ";

        // Ensure the response starts with the expected prefix
        if response.starts_with(prefix) {
            // Find the position of the opening bracket '(' after the prefix
            if let Some(start) = response.iter().position(|&b| b == b'(') {
                // Find the position of the closing bracket ')' after the opening bracket
                if let Some(end) = response.iter().position(|&b| b == b')') {
                    // Ensure the closing bracket comes after the opening bracket
                    if end > start {
                        // Extract the bytes within the brackets
                        let inside_brackets = &response[start + 1..end];
                        // Extract the first digit and parse it
                        if let Some(&digit) = inside_brackets.iter().find(|&&b| b.is_ascii_digit())
                        {
                            match digit {
                                b'0' => return Ok(PdpState::Deactivated),
                                b'1' => return Ok(PdpState::Activated),
                                _ => return Err(Error::InvalidState),
                            }
                        }
                    }
                }
            }
        }

        Err(Error::ParseError)
    }
}

pub fn split_nested(bytes: &[u8], separator: Option<&[u8]>) -> (Vec<Vec<u8>>, Vec<u8>) {
    let mut result = Vec::new();
    let mut result_final = Vec::new();
    result_final.push(b'<');
    let mut start = 0;
    let delimiter = [13, 10, 13, 10];

    // const SEPARATOR: &[u8] = b"AT+CIPSTATUS\r\n";
    // while let Some(end) = bytes[start..]
    //     .windows(delimiter.len())
    //     .position(|window| window == delimiter)
    // {
    //     result.push(bytes[start..start + end].to_vec());
    //     start += end + delimiter.len();
    // }

    loop {
        match bytes[start..]
            .windows(delimiter.len())
            .position(|window| window == delimiter)
        {
            Some(end) => {
                if let Some(index) = bytes[start..start + end]
                    .windows(separator.unwrap().len())
                    .position(|window| window == separator.unwrap())
                {
                    let (part1, part2) =
                        bytes[start..start + end].split_at(index + separator.unwrap().len());
                    // (part1, part2)
                    result.push(part1.to_vec());
                    result.push(part2.to_vec());
                    start += end + delimiter.len();
                } else {
                    // (response, &[])
                    result.push(bytes[start..start + end].to_vec());
                    start += end + delimiter.len();
                }
            }
            None => break,
        }
    }

    // Push the remaining part after the last delimiter
    if start < bytes.len() {
        result.push(bytes[start..].to_vec());
        // if bytes.len() > 1 {
        // result.push(bytes[..delimiter[..2].len()].to_vec());
        // } else
    }
    // debug_println!("nested_chunks: {:?}", result);

    // result
    // Add <> at the end of the array and use * to join the nested array
    for slice in &result {
        if !result_final.is_empty() {
            result_final.push(b'*');
        }
        result_final.extend_from_slice(&slice[..]);
    }
    result_final.push(b'>');

    (result, result_final)
}
#[allow(unused)]
fn join_and_wrap(nested_slice: &[&[u8]]) -> Vec<u8> {
    let mut result = Vec::new();
    result.push(b'<');

    for slice in nested_slice {
        if !result.is_empty() {
            result.push(b'*');
        }
        result.extend_from_slice(slice);
    }

    result.push(b'>');
    result
}

pub fn replace_crlf_with_lf(bytes: &[u8]) -> Vec<u8> {
    let mut result = Vec::new();
    let mut i = 0;

    while i < bytes.len() {
        if i < bytes.len() - 1 && bytes[i] == 13 && bytes[i + 1] == 10 {
            result.push(10); // Append LF
            i += 2; // Skip CR and LF
        } else {
            result.push(bytes[i]);
            i += 1;
        }
    }

    result
}

impl core::fmt::Debug for ModemResponse {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let cleaned = clean_array::remove_invalid_utf8(&self.command_sent[..]);

        let command_sent_str = core::str::from_utf8(&cleaned).unwrap_or("<invalid utf-8>");
        f.debug_struct("ModemResponse")
            .field("command_sent", &command_sent_str)
            .field("response", &self.response)
            .finish()
    }
}

impl fmt::Debug for Status {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // let command_sent_str =
        //     core::str::from_utf8(&self.command_sent).unwrap_or("<invalid utf-8>");
        match self {
            Status::OK => write!(f, "OK"),
            Status::CONNECT => write!(f, "CONNECT"),
            Status::ModemError(Some(e)) => {
                // let cleaned = clean_array::remove_invalid_utf8(&bytes[..]);
                // let error_bytes = core::str::from_utf8(&cleaned).unwrap_or("<invalid utf-8>");
                // write!(f, "ModemError({:?})", error_bytes)
                write!(f, "ModemError({:?})", e)
            }
            Status::ModemError(None) => write!(f, "ModemError(None)"),
        }
    }
}

pub mod clean_array {
    use alloc::vec::Vec;
    const KEY_LEFTSHIFT: u8 = 42;
    const KEY_RIGHTSHIFT: u8 = 54;
    // const CHAR_LESS_THAN: u8 = b'<'; // ASCII value for '<'
    // const CHAR_GREATER_THAN: u8 = b'>'; // ASCII value for '>'
    const CHAR_LESS_THAN: u8 = 60; // ASCII value for '<'
    const CHAR_GREATER_THAN: u8 = 62; // ASCII value for '>'
    const CR: u8 = 13; // Carriage Return
    const LF: u8 = 10; // Line Feed

    pub fn remove_invalid_utf8(input: &[u8]) -> Vec<u8> {
        let mut output = Vec::with_capacity(input.len());
        let mut i = 0;
        while i < input.len() {
            if input[i] <= 127 {
                // ASCII character
                output.push(input[i]);
                i += 1;
            } else if i + 1 < input.len() && is_valid_utf8_start(input[i]) {
                // Potential start of a multi-byte sequence
                let char_len = utf8_char_length(input[i]);
                if i + char_len <= input.len() && is_valid_utf8_sequence(&input[i..i + char_len]) {
                    // Valid multi-byte sequence
                    output.extend_from_slice(&input[i..i + char_len]);
                    i += char_len;
                } else {
                    // Invalid sequence, skip this byte
                    i += 1;
                }
            } else {
                // Invalid byte, skip it
                i += 1;
            }
        }
        output
    }

    // A function to remove shift keys
    pub fn remove_chars_and_add_newline(input: &[u8]) -> Vec<u8> {
        let mut output: Vec<u8> = input
            .iter()
            .filter(|&&byte| {
                byte != KEY_LEFTSHIFT
                    && byte != KEY_RIGHTSHIFT
                    && byte != CHAR_LESS_THAN
                    && byte != CHAR_GREATER_THAN
            })
            .cloned()
            .collect();

        // Check if [13, 10] already exists at the end
        if !output.ends_with(&[CR, LF]) {
            // If [13, 10] doesn't exist at the end, check for [13]
            if output.ends_with(&[CR]) {
                // If it ends with [13], just add [10]
                output.push(LF);
            } else {
                // If it doesn't end with [13], add both [13, 10]
                output.extend_from_slice(&[CR, LF]);
            }
        }

        output
    }

    // Function to combine remove_shift_keys and remove_invalid_utf8 operations if needed
    pub fn clean_input(input: &[u8]) -> Vec<u8> {
        let without_shift = remove_chars_and_add_newline(input);
        remove_invalid_utf8(&without_shift)
    }

    fn is_valid_utf8_start(byte: u8) -> bool {
        byte & 0b1100_0000 == 0b1100_0000
    }

    fn utf8_char_length(byte: u8) -> usize {
        if byte & 0b1000_0000 == 0 {
            1
        } else if byte & 0b1110_0000 == 0b1100_0000 {
            2
        } else if byte & 0b1111_0000 == 0b1110_0000 {
            3
        } else if byte & 0b1111_1000 == 0b1111_0000 {
            4
        } else {
            1
        } // Invalid start byte, treat as 1-byte char
    }

    fn is_valid_utf8_sequence(seq: &[u8]) -> bool {
        match seq.len() {
            2 => seq[1] & 0b1100_0000 == 0b1000_0000,
            3 => seq[1] & 0b1100_0000 == 0b1000_0000 && seq[2] & 0b1100_0000 == 0b1000_0000,
            4 => {
                seq[1] & 0b1100_0000 == 0b1000_0000
                    && seq[2] & 0b1100_0000 == 0b1000_0000
                    && seq[3] & 0b1100_0000 == 0b1000_0000
            }
            _ => false,
        }
    }
}
