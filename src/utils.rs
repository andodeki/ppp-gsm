extern crate rand;
extern crate rand_core; // or your preferred panic handler

use rand::rngs::SmallRng;
use rand_core::RngCore;
use rand_core::SeedableRng;

#[macro_export]
macro_rules! singleton {
    ($val:expr, $T:ty) => {{
        static STATIC_CELL: ::static_cell::StaticCell<$T> = ::static_cell::StaticCell::new();
        STATIC_CELL.init($val)
    }};
}

// Dummy entropy source for demonstration purposes
struct DummyEntropySource;

impl DummyEntropySource {
    fn new() -> Self {
        DummyEntropySource
    }

    fn get_entropy(&self) -> [u8; 16] {
        // In a real `no_std` environment, you need to replace this with a proper entropy source.
        [0x42; 16] // Just an example, replace with real entropy
    }
}

pub fn generate_random_seed() -> u64 {
    let entropy_source = DummyEntropySource::new();
    let seed = entropy_source.get_entropy();

    let mut rng = SmallRng::from_seed(seed);

    // Generate random seed
    let mut seed_bytes = [0u8; 8];
    rng.fill_bytes(&mut seed_bytes);

    u64::from_le_bytes(seed_bytes)
}
